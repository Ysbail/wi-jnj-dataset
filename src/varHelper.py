import numpy as np
import pandas as pd

def createVar_min_mean_max(df_series,colname):
    
    nameCol=colname
    nameColMin=colname+'_min'
    nameColMean=colname+'_mean'
    nameColMax=colname+'_max'
    
    df_filterMax = df_series.groupby(pd.Grouper(freq='D')).agg({nameCol : np.nanmax})
    # df_filterMax.reset_index(inplace=True)
    df_filterMax.rename(columns={nameCol: nameColMax},inplace=True)
    
    df_filterMin = df_series.groupby(pd.Grouper(freq='D')).agg({nameCol : np.nanmin})
    # df_filterMin.reset_index(inplace=True)
    df_filterMin.rename(columns={nameCol: nameColMin},inplace=True)
   
    df_filterMean = df_series.groupby(pd.Grouper(freq='D')).agg({nameCol : np.nanmean})
    # df_filterMean.reset_index(inplace=True)
    df_filterMean.rename(columns={nameCol: nameColMean},inplace=True)
    
    # del(df_filterMax['Date'])
    # del(df_filterMean['Date'])
    # df=pd.concat([df_filterMin, df_filterMax], axis = 1)
    # df=pd.concat([df, df_filterMean], axis = 1)

    df = pd.concat((df_filterMin, df_filterMax, df_filterMean), axis=1)

    return df


def createVarPrec(df_series,colname):
    
    nameCol=colname
    nameColAcum=colname+'_acumDiario'
    
    df_filterAC=df_series.groupby(pd.Grouper(freq='D')).agg({nameCol : np.sum})
    # df_filterAC.reset_index(inplace=True)
    df_filterAC.rename(columns={nameCol: nameColAcum},inplace=True)
    
    return df_filterAC


def maiorPQuantil(p,x):
    if (x>p): return 1
    else: return 0
    

def temperaturaPontoOrvalho(df):
    
    T=df['temp_mean']
    UR=df['umid_rel_mean']
    tpo=T-((100-UR)/5)
    return tpo
    
    
def sensacaoTermica(df):
    T = df['temp_max']
    W = df['wind_max']
    #WindChill=(12.1452+11.6222*np.sqrt(W)-1.16222*W)*(33*T)
    WindChill = 33 + (10 * np.sqrt(W) + 10.45 - W) * (T - 33) / 22
    return WindChill


def heat_index_noaa(df):
    #validado com NOAA https://www.wpc.ncep.noaa.gov/html/heatindex.shtml
    temp=df['temp_mean']
    humi=df['umid_rel_mean']
    #passar para F
    temp=(temp * 1.8) + 32
    HI = 0.5 * (temp + 61.0 + ((temp-68.0)*1.2) + (humi*0.094))
    if(HI>=80):
        HI = -42.379 + 2.04901523*temp + 10.14333127*humi - 0.22475541*temp*humi - 0.00683783*temp*temp - 0.05481717*humi*humi + 0.00122874*temp*temp*humi + 0.00085282*temp*humi*humi - 0.00000199*temp*temp*humi*humi
        if((humi< 13) & (80<temp<112)):
            ADJUSTMENT = ((13-humi)/4)*np.sqrt((17-np.absolute(temp-95))/17)
            HI=HI-ADJUSTMENT
        if( (humi > 85) & (80<temp<87)):
            ADJUSTMENT = ((humi-85)/10) * ((87-temp)/5)
            HI=HI+ADJUSTMENT
    # passar para celsius
    HI=(HI - 32) / 1.8
    return HI


def wind_speed(df):
    return np.linalg.norm(df[['u10', 'v10']].values, axis=1)


def agruparCidades(x):
    if ((x=='DIADEMA') or (x=='OSASCO') or (x=='SANTO ANDRE') or (x=='SAO BERNARDO DO CAMPO') or (x=='SAO CAETANO DO SUL')):
        return 'SAO PAULO'
    else: return x


def agrupamentoSemanal(df):
    df['dias_Temp_p90']=df['temp_max'].apply(lambda x: maiorPQuantil(df['temp_max'].quantile(0.9),x))
    df['dias_Temp_p75']=df['temp_max'].apply(lambda x: maiorPQuantil(df['temp_max'].quantile(0.75),x))
    df['dias_Temp_p50']=df['temp_max'].apply(lambda x: maiorPQuantil(df['temp_max'].quantile(0.50),x))
    df['dias_Prec_dp90']=df['prec_acumDiario'].apply(lambda x: maiorPQuantil(df['prec_acumDiario'].quantile(0.9),x))
    df['dias_Prec_dp75']=df['prec_acumDiario'].apply(lambda x: maiorPQuantil(df['prec_acumDiario'].quantile(0.75),x))
    df['dias_Prec_dp50']=df['prec_acumDiario'].apply(lambda x: maiorPQuantil(df['prec_acumDiario'].quantile(0.5),x))
    df['dias_Hr_dp90']=df['umid_rel_max'].apply(lambda x: maiorPQuantil(df['umid_rel_max'].quantile(0.9),x))
    df['tempPontoOrvalho']=df.apply(lambda x: temperaturaPontoOrvalho(x), axis=1)
    # df['sensTermica']=df.apply(lambda x: sensacaoTermica(x),axis=1)
    
    aggregations = {
    'un_sundown_body':'sum',
    'un_neutrog_face':'sum',
    'temp_min':'min',
    'temp_mean': 'mean',
    'temp_max': 'max',
    'dias_Temp_p90': 'sum',
    'dias_Temp_p75': 'sum',
    'dias_Temp_p50': 'sum',
    'dias_Prec_dp90': 'sum',
    'dias_Prec_dp75': 'sum',
    'dias_Prec_dp50': 'sum',
    'dias_Hr_dp90': 'sum',
    'prec_acumDiario':'sum',
    'tempPontoOrvalho':'mean',
    'sensTermica':'max',
    'umid_rel_min':'min',
    'umid_rel_mean':'mean',
    'umid_rel_max':'max',
    'wind_min':'min',
    'wind_mean':'mean',
    'wind_max':'max',
    'dewpoint_min':'min',
    'dewpoint_mean':'mean',
    'dewpoint_max':'max',
    'press_min': 'min',
    'press_max': 'max',
    'press_mean': 'mean',
    'heat_index': 'mean'
    }
    
    # print([i for i in aggregations.keys() if not i in df.columns])
    df_ = df.resample('W-mon').agg(aggregations)
    
    return df_