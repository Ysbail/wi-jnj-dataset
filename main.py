import json, collections, os
import numpy as np
import pandas as pd
import netCDF4 as nc

from src.varHelper import createVar_min_mean_max, createVarPrec, sensacaoTermica, heat_index_noaa, agruparCidades, wind_speed, agrupamentoSemanal
import src.ncHelper as ncHelper

json_path = './config_paths.json'
CONF = json.load(open(json_path, 'r'))

cidades = CONF['_cidades']
years = ['2018', '2019', '2020', '2021']
variable_files_func = [('t2m', '2m_temperature_{year}.nc', createVar_min_mean_max, lambda x: x - 273.15),
                       ('d2m', '2m_dewpoint_temperature_{year}.nc', createVar_min_mean_max, lambda x: x - 273.15),
                       ('r', 'relative_humidity_1000hPa_{year}.nc', createVar_min_mean_max, lambda x: x),
                       ('sp', 'surface_pressure_{year}.nc', createVar_min_mean_max, lambda x: x),
                       ('tp', 'total_precipitation_{year}.nc', createVarPrec, lambda x: x * 1000.)]
wind_files = {'u': ('u10', '10m_u_component_of_wind_{year}.nc'), 
              'v': ('v10', '10m_v_component_of_wind_{year}.nc')}
target_names = {'t2m': 'temp', 'd2m': 'dewpoint', 'r': 'umid_rel', 'sp': 'press', 'tp': 'prec'}

# load jnj
df_jnj = pd.read_csv(CONF['_jnj'], delimiter=',') # use most recent dataset possible
df_jnj['Date'] = pd.to_datetime(df_jnj['Date']) # .to_pydatetime()
df_jnj['weekday'] = df_jnj['Date'].apply(lambda x: x.weekday())
df_jnj['Month'] = df_jnj['Date'].apply(lambda x: x.month)
df_jnj = df_jnj[(df_jnj['Estado']!='PB') & (df_jnj['Cidade']!='SANTO ANDRE')]
df_jnj['Cidade'] = df_jnj['Cidade'].apply(lambda x: agruparCidades(x))
df_jnj = df_jnj.groupby(['Date', 'Week', 'Year', 'WeekKey', 'Cidade',
       'Marca', 'CATE', 'weekday', 'Month'])["Unidades"].sum().to_frame(name = 'Unidades').reset_index()
df_jnj.set_index('Date', inplace=True)


print('creating main dataframes')
cidades_df = collections.defaultdict(pd.DataFrame)
for variable, file, func, var_corr in variable_files_func:
    print(variable)
    year_df = collections.defaultdict(pd.DataFrame)
    for year in years:
        # convert 3 lines into function -> returns grid/nc_file, time, lat, lon
        nc_file = nc.Dataset(os.path.join(CONF['_era5'], file.format(year=year)), 'r', keepweakref=True) # open file to get multiple needed points
        nc_time = ncHelper.load_time(nc_file.variables['time'])
        lats, lons = ncHelper.get_lats_lons_file(nc_file, skip=1, trim=0)
        for cidade, (lat, lon) in cidades.items():
            ilat, ilon = ncHelper.get_idx_pos(lat, lon, lats, lons)
            _df = ncHelper.nc2df(nc_file, variable, nc_time, pos=(ilat, ilon))
            _df.rename(columns=target_names, inplace=True)
            _df = func(_df, target_names[variable])
            _df = _df.apply(var_corr)
            year_df[cidade] = pd.concat((year_df[cidade], _df))
    for cidade in cidades.keys():
        cidades_df[cidade] = pd.concat((cidades_df[cidade], year_df[cidade]), axis=1)
        # print(cidades_df[cidade].head())
        # print(cidades_df[cidade].tail())

print('defining wind speed')            
# wind_speed
wind_df = collections.defaultdict(pd.DataFrame)
for _dir in ['u', 'v']:
    year_df = collections.defaultdict(pd.DataFrame)
    for year in years:
        nc_file = nc.Dataset(os.path.join(CONF['_era5'], wind_files[_dir][1].format(year=year)), 'r', keepweakref=True)
        nc_time = ncHelper.load_time(nc_file.variables['time'])
        lats, lons = ncHelper.get_lats_lons_file(nc_file, skip=1, trim=0)
        for cidade, (lat, lon) in cidades.items():
            ilat, ilon = ncHelper.get_idx_pos(lat, lon, lats, lons)
            _df = ncHelper.nc2df(nc_file, wind_files[_dir][0], nc_time, pos=(ilat, ilon))
            year_df[cidade] = pd.concat((year_df[cidade], _df))
    for cidade in cidades.keys():
      wind_df[cidade] = pd.concat((wind_df[cidade], year_df[cidade]), axis=1)

print('resampling wind')            
# apply resampled wind_speed
for cidade in cidades.keys():
    # print(cidade)
    wind_df[cidade]['wind'] = wind_speed(wind_df[cidade]) # .apply(wind_speed, axis=1)
    # print(wind_df[cidade].head())
    _df = createVar_min_mean_max(wind_df[cidade][['wind']], 'wind')
    cidades_df[cidade] =  pd.concat((cidades_df[cidade], _df), axis=1)

print('adding derivated variables')    
# rename variables as needed
# add derivated data
for cidade, df in cidades_df.items():
    # df.rename(columns=target_names, inplace=True)
    # sensTermica, heat_index
    df['sensTermica'] = df.apply(sensacaoTermica, axis=1)
    df['heat_index'] = df.apply(heat_index_noaa, axis=1)
    
    # add jnj data
    df_sb = df_jnj.loc[(df_jnj['Marca'] == 'SUNDOWN') & (df_jnj['CATE'] == 'Body') & (df_jnj['Cidade'] == cidade)].rename(columns={'Unidades': 'un_sundown_body'})[['un_sundown_body']]
    df_nf = df_jnj.loc[(df_jnj['Marca'] == 'NEUTROGENA') & (df_jnj['CATE'] == 'Face') & (df_jnj['Cidade'] == cidade)].rename(columns={'Unidades': 'un_neutrog_face'})[['un_neutrog_face']]

    df = pd.concat((df, df_sb, df_nf), axis=1)
    
    df.dropna(inplace=True)
    if cidade in df_jnj['Cidade'].values:
        df.to_csv('./output/%s_full.csv'%(cidade.lower().replace(' ', '_')))
        agrupamentoSemanal(df).to_csv('./output/%s_full_weekly.csv'%(cidade.lower().replace(' ', '_')))
        # resample weekly data
        # to_csv_weekly


